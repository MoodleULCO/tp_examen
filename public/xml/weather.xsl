<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="700px" height="500px">
            <xsl:choose>
                <xsl:when test="//ClientError">
                    <text x="10" y="30" font-size="30" id="weather">La ville choisie n'existe pas</text>
                </xsl:when>
                <xsl:otherwise>
                    <text x="10" y="30" font-size="30" id="weather"><xsl:value-of select="//city/@name"/> (<xsl:value-of select="//city/country"/>)</text>
                    <text x="10" y="80" font-size="30" id="weather"><xsl:value-of select="//weather/@value"/></text>
                    <image href="http://openweathermap.org/img/wn/{//weather/@icon}@2x.png" x="300" y="45" width="60" height="50" />
                    <text x="10" y="130" font-size="30" id="weather"><xsl:value-of select="//temperature/@value"/>°
                    <xsl:if test="//temperature/@unit = 'celsius'">
                    C
                    </xsl:if>
                    <xsl:if test="//temperature/@unit = 'kelvin'">
                    F
                    </xsl:if>
                    </text>
                </xsl:otherwise>
            </xsl:choose>
        </svg>
    </xsl:template>
</xsl:stylesheet>
