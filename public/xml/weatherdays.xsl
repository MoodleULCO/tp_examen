<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="700" height="250">
            <text x="80" y="110" text-anchor="middle">Temperature</text>
            <line x1="80" y1="120" x2="80" y2="200"/>
            <line x1="80" y1="140" x2="85" y2="140"/>
            <line x1="80" y1="160" x2="85" y2="160"/>
            <line x1="80" y1="180" x2="85" y2="180"/>
            <line x1="80" y1="200" x2="380" y2="200"/>
            <text x="400" y="210" text-anchor="middle">Jours</text>
            <text x="70" y="200" text-anchor="middle">0</text>
            <text x="70" y="180" text-anchor="middle">10</text>
            <text x="70" y="160" text-anchor="middle">20</text>
            <text x="70" y="140" text-anchor="middle">30</text>
            <polyline>
                <xsl:attribute name="points">
                    <xsl:for-each select="//time">
                        <xsl:if test="position() != count(//time)">
                            <xsl:value-of select="(((380-80) div 40)*position()-1) +80"/>,<xsl:value-of select="200-(temperature/@value div ((200-120) div 40))"/>
                            <xsl:text> </xsl:text>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:attribute>
            </polyline>
        </svg>
    </xsl:template>
</xsl:stylesheet>