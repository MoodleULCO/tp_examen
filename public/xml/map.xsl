<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <iframe width="75%" 
            height="500" 
            frameborder="0" 
            scrolling="no" 
            marginheight="0" 
            marginwidth="0" 
            style="border: 1px solid black"
            src="http://www.openstreetmap.org/export/embed.html?bbox={//place[1]/@boundingbox}&amp;marker={//place[1]/@lat}%2C{//place[1]/@lon}">
        </iframe>       
    </xsl:template>
</xsl:stylesheet>
