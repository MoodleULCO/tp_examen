let openWeatherMapUrl = "";
let nominatimURL = "";
let city = "";
let unit = "";

/*NE PAS TOUCHER */
function get(url, mode='xml') {
 return new Promise(function (resolve, reject) {
   const xhr = new XMLHttpRequest();
   xhr.open('GET', url);
   xhr.onload = _ => resolve(
     (mode === 'xml') ? xhr.responseXML : xhr.responseText
   );
   xhr.send();
 });
}
/*********************/

//Retourne le résultat d'une expression XPath en JS
//NE PAS TOUCHER 
function query(xmlDoc, expr){
    var nodes = xmlDoc.evaluate(expr, xmlDoc, null, XPathResult.ANY_TYPE, null);
    return(nodes.iterateNext().nodeValue);
}

//Retourne le résultat d'une transformation XSLT
//NE PAS TOUCHER 
function getFragment(xml, xsl){
    let xsltProcessor = new XSLTProcessor();
    xsltProcessor.importStylesheet(xsl);

    return(xsltProcessor.transformToFragment(xml, document));
}

//Modifier un attribut XSL
function setXSL(xsl, attribute, value){
  proc = new XSLTProcessor();
  proc.importStylesheet(xsl);

  proc.setParameter(null, attribute, value);
}

/*
 OpenWeatherMap API Keys
 1) 9cd4ed2efdb62088151c9aed6e319657
 2) 7e2ba55741f02b763cfaf7adc7dfacc5
 3) dc9847c95df0905b0e859c8ed7a5707f
*/

/*TODO */
async function changeWeatherAndMap(e, updated){
  if(updated != true){
    city = document.getElementById("localisation").value;
    document.getElementById("localisation").value = "";

    if(document.getElementById("Fahr").checked){
      unit = "";
    }else{
      unit = "units=metric";
    }
  }
  console.log(city);

  openWeatherMapUrl = `http://api.openweathermap.org/data/2.5/weather?q=`+city+`&mode=xml&`+unit+`&APPID=9cd4ed2efdb62088151c9aed6e319657&lang=fr`;
  openWeatherMapDaysUrl = `http://api.openweathermap.org/data/2.5/forecast?q=`+city+`&lang=fr&mode=xml&units=metric&APPID=9cd4ed2efdb62088151c9aed6e319657`;
  nominatimURL = `https://nominatim.openstreetmap.org/search?q=`+city+`&format=xml`;
  
  console.log(nominatimURL);
  console.log(openWeatherMapUrl);
  
  //****************************************   // 
  //**** NE PAS TOUCHER ********************   // 
  const nominatimXML = await get(nominatimURL,mode='xml');
  const openWeatherXML = await get(openWeatherMapUrl, mode='xml');
  const openWeatherDaysXML = await get(openWeatherMapDaysUrl, mode="xml");
  
  const xslWeather = await get( "/xml/weather.xsl", mode='xml');
  const xslMap = await get( "/xml/map.xsl", mode='xml');
  const xslMapDays = await get("/xml/weatherdays.xsl", mode='xml');

  let nominatimFragment  = getFragment(nominatimXML, xslMap); 
  let weatherFragment  = getFragment(openWeatherXML, xslWeather);
  let weatherdaysFragment = getFragment(openWeatherDaysXML, xslMapDays);

  while(map.firstChild) map.removeChild(map.firstChild);
  while(meteo.firstChild) meteo.removeChild(meteo.firstChild);
  while(meteodays.firstChild) meteodays.removeChild(meteodays.firstChild);

  document.getElementById("map").appendChild(nominatimFragment);
  document.getElementById("meteo").appendChild(weatherFragment);
  document.getElementById("meteodays").appendChild(weatherdaysFragment);
  //****************************************  // 
  //****************************************  // 

  setXSL(xslMap, "url", nominatimURL);
}

async function updateInfo(e){
  if(document.getElementById("Fahr").checked){
    unit = "";
  }else{
    unit = "units=metric";
  }
  changeWeatherAndMap(e, true);
}

localisation.onchange = changeWeatherAndMap;
document.querySelector("#Cel").addEventListener("click",updateInfo);
document.querySelector("#Fahr").addEventListener("click",updateInfo);

//&layer=mapnik&marker=<xsl:value-of select="{//place[1]/@lat}"/>%2C<xsl:value-of select="{//place[1]/@lon}"/>